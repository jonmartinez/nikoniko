<?php
	include '../../BD/Conexion.php';
    include '../../BD/funcionesbd.inc.php';
    require '../../dao/Equipo.php';
    require '../../dao/Animo.php';
    session_start();
    $accion="";
    $url="";
    
    if(!isset($_SESSION["IDUSUARIO"])) {header ("Location: index.html"); die();}
    if(!empty($_POST))
    {
        $conexion=Conexion::conectar();
        foreach($_POST as $variable => $valor) ${$variable}=$valor;
        switch ($accion)
        {
            case 'listarEquipos':
                ?>
                	<ul class="todo-list">
                  
		                <?php
		                    $xs=Equipo::getEquiposProyecto(array('IDP'=>$idproyecto), $conexion);
		                    foreach ($xs as $x) 
		                    {
		                        echo "<li>";
		                        echo '<!-- drag handle -->
		                                <span class="handle">
		                                  <i class="fa fa-ellipsis-v"></i>
		                                  <i class="fa fa-ellipsis-v"></i>
		                                </span>
		                                <!-- checkbox -->
		                                <input type="checkbox" value="'.$x['id'].'" name="id"/>';
		                        
		                        echo '<span class="text"><b>'.$x['proyecto'].':</b> '.$x['equipo'].'</span>';
		                        //echo "<td>".$usuario['estado']."</td>";
		                        echo '<!-- Emphasis label -->
		                                <!-- General tools such as edit or delete-->
		                                <div class="tools">';
		                                  echo "<i class='fa fa-edit btn-mostrar-formulario'"
		                                .'data-toggle="modal" data-target="#formulario-modal"'
		                                . "data-accion='Modificar'"
		                                .'data-url="'. $Archivo .'"'
		                                ."data-token='".encriptar($x['id'])."'"
		                                ."data-title='Modificar Usuario'></i>";
		                                echo "<i class='fa fa-trash-o Eliminar'"
		                                    .'data-url="'. $Archivo .'"'
		                                    ."data-token='".encriptar($x['id'])."'"
		                                    . ">";
		                                echo "</i>";
		                        echo'</div>';
		                        echo "</li>";
		                    }
		                ?>
		            </ul>
                <?php
                break;



                case 'estadosEquipo':
                	echo '<div id="myfirstchart"></div>';
		                $xs=Animo::getEstadosDeUsuariosDeUnEquipo($idequipo,$fechamin,$fechamax,$conexion);
		                $datos='';
		                $fechaAnterior='';
		                $j=0;
		                $i=0;
		                $num=count($xs);
		                //print_r(json_encode($xs));
		                foreach ($xs as $x) 
		                {
		                  //para el primer registro imprimimos el estado de animo
		                  //echo "j--$j---";
		                   $i=($fechaAnterior==$x['fecha'])?$i+1:0;
		                  //echo "-------".$x['fecha']."<br>";
		                  
		                  if($i==0 && $j==0) 
		                  {
		                    $datos.="{month:'".$x['fecha']."',user".$x['id'].":".$x['animo'];
		                  }else if($i==0 && $j>0)
		                  {
		                    //final de registro
		                    $datos.="},{month:'".$x['fecha']."',user".$x['id'].":".$x['animo'];
		                  }else
		                  {
		                    $datos.=",user".$x['id'].":".$x['animo'];
		                  }
		                  if($num==($j+1)){$datos.="},";}
		                  $fechaAnterior=$x['fecha'];
		                  $j++;
		                }
		                //echo substr(trim($datos), 0, strlen($datos)-1);



		                //llaves de referencia
		                $xs1=Animo::getUsuariosDeUnEquipo($idequipo,$fechamin,$fechamax,$conexion);
		                $keys='';
		                $labels='';
		                foreach ($xs1 as $x1) 
		                {
		                  $keys.="'user".$x1['id']."',";
		                  $labels.="'".$x1['user']."',";
		                }
		                //echo $keys=substr(trim($keys), 0, strlen($keys)-1);//llaves de referencia
		                //echo $labels=substr(trim($labels), 0, strlen($labels)-1);
		              ?>
		                
		              </div><!-- /.row (main row) -->
		              <script type="text/javascript">
		                 new Morris.Line({
		                  element: 'myfirstchart',
		                  data: [<?php echo substr(trim($datos), 0, strlen($datos)-1);?>],
		                  xkey: 'month',
		                  ykeys: [<?php echo $keys;?>],
		                  labels: [<?php echo $labels;?>],
		                  ymax:5,
		                  ymin:0,
		                  goals:[1,4],
		                  goalLineColors:['#d14','#02e809']
		                }).on('click', function(i, row){
		                	row.accion='darDatosAnimoLider';
		                	//console.log(i,row);
							$.ajax({
							url: 'lib/ajax/utilidades.php',
							type: 'POST',
							data: row,
							dataType: 'html'
							})
				          	.done(function(data)
				          	{
				               $('#datosanimos-title').html(''); // blank before load.
				               $('#datosanimos-title').html('Estado de animo para la fecha '+row.month); // load here
				               $('#datosanimos').html(''); // blank before load.
				               $('#datosanimos').html(data); // load here
				               $('#datosanimos-modal').modal('show');
				          	})
				          	.fail(function()
				          	{
				               $('#datosanimos').html('Error al cargar los datos');
				          	});
						});
		              </script>
		              <?php
                	break;


             case 'selectEquipo':
             	?>
             		<label for="idequipo">Seleccione un equipo:</label>
             		<select class="selectpicker" title="Seleccione un equipo" name="idequipo" required>
             			<?php
             				$xs=Equipo::getEquiposProyecto(array('IDP'=>$idproyecto), $conexion);
             				foreach ($xs as $x) 
             				{
             					echo '<option value='.$x['id'].'>'.$x['equipo'].'</option>';
             				}
             			?>
             		</select>
             	<?php
             	break;
             case 'darDatosAnimoLider':
             	$condicional='';
             	foreach($_POST as $variable => $valor)
             	{
             		//echo $variable .'<br>';
             		$condicional.=(strpos($variable, 'user')!==false)?' id='.str_replace('user','',$variable). ' or':'';	
             	} 
             	$condicional=substr(trim($condicional), 0, strlen($condicional)-4);
             	$cadena="SELECT concat(nombres,' ',apellidos) user,animo,detalle FROM `animos`
				inner join usuarios on (id=idusuario and ($condicional))
				where fecha='$month'";
             	$sql = $conexion->prepare($cadena);
            	$sql->execute();
            	$resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            	echo "<table class='table table-striped table-hover'>";
            	echo "<thead>";
            	echo "<tr><th>Estado</th><th>Usuario</th><th>Detalle</th></tr>";
            	echo "</thead>";
            	echo "<tbody>";
              	foreach ($resultado as $x) 
            	{
            		$danger=($x['animo']<=1)?'class="table-danger"':'';
            		echo "<tr $danger><td><img src='lib/emoticones/".$x['animo'].".png' width='30px'></td><td>".$x['user']."</td><td>".$x['detalle']."</td></tr>";
            	}
            	echo "</tbody>";
            	echo "</table>";

             	break;
             case 'darDatosAnimoUsuario':
             	$condicional='id='.$_SESSION["IDUSUARIO"];
             	$cadena="SELECT concat(nombres,' ',apellidos) user,animo,detalle FROM `animos`
				inner join usuarios on (id=idusuario and ($condicional))
				where fecha='$month'";
             	$sql = $conexion->prepare($cadena);
            	$sql->execute();
            	$resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            	echo "<table class='table table-striped table-hover'>";
            	echo "<thead>";
            	echo "<tr><th>Estado</th><th>Usuario</th><th>Detalle</th></tr>";
            	echo "</thead>";
            	echo "<tbody>";
              	foreach ($resultado as $x) 
            	{
            		$danger=($x['animo']<=1)?'class="table-danger"':'';
            		echo "<tr $danger><td><img src='lib/emoticones/".$x['animo'].".png' width='30px'></td><td>".$x['user']."</td><td>".$x['detalle']."</td></tr>";
            	}
            	echo "</tbody>";
            	echo "</table>";

             	break;
        }
        
        
        $conexion=null;
    }
?>