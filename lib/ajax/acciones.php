<?php
    include '../../BD/Conexion.php';
    include '../../BD/funcionesbd.inc.php';
    session_start();
    $accion="";
    $url="";
    
    if(!isset($_SESSION["IDUSUARIO"])) {header ("Location: index.html"); die();}
    if(!empty($_POST))
    {
        $conexion=Conexion::conectar();
        foreach($_POST as $variable => $valor) ${$variable}=$valor;
        $des=explode('.', desencriptar($url));
        $archivo="";
        switch ($accion)
        {
            case 'formulario':
                $archivo=$des[0].'formulario.'.$des[1]; 
                break;
            
            case 'Guardar' || 'Modificar' || 'Eliminar':
               $archivo=$des[0].'actualizar.'.$des[1]; 
                break;
        }
        include ("../../".$archivo);
        
        $conexion=null;
    }   
?>