$(document).ready(function()
{
    $(document).on('click', '.btn-mostrar-formulario', function(e)
     {
        //limpirar el contenido de modal
        //alert("#btn-mostrar-formulario");
        $('input[type="submit"]').removeAttr('disabled');
        $('#modal-formulario-body').text('');
        $('#modal-formulario-title').text('');
        $('#modal-formulario-title').text($(this).data(('title')));
        $('#accion-frm').val($(this).data('accion'));
        //variables
        var url = $(this).data('url'); // get id of clicked row
        var accion = $(this).data('accion');
        var titulo = $(this).data('title');
        var mydata={"url":url,"accion":"formulario"};
        if($(this).data('token')!='') mydata={"url":url,"accion":"formulario","token":$(this).data('token')};
        $.ajax({
           url: 'lib/ajax/acciones.php',
           type: 'POST',
           data: mydata,
           dataType: 'html'
        })
        .done(function(data)
        {
             $('#modal-formulario-body').html(''); // blank before load.
             $('#modal-formulario-body').html(data); // load here
             $('#accion').val(accion);
             $('.selectpicker').selectpicker('show');

        })
        .fail(function()
        {
             $('#modal-formulario-body').html('<i class="glyphicon glyphicon-info-sign"></i> Ocurrio un error, por favor intente nuevamente...');
        });
               
          //}, 5000);
     }).on('click', '.Eliminar', function(e)
     {
          //doSomethingCool();
          if(confirm('Realmente desea eliminar este registro'))
          {
                e.preventDefault();
                var url = $(this).data('url'); // get id of clicked row
                var mydata={"url":url,"accion":"Eliminar","token":$(this).data('token')};
                $.ajax({
                    url: 'lib/ajax/acciones.php',
                    type: 'POST',
                    data: mydata,
                    dataType: 'html'
                })
                .done(function(data)
                {
                    $('#contenido-ajax').html(''); // blank before load.
                    $('#contenido-ajax').html(data); // load here
                    $('.selectpicker').selectpicker('refresh');
                })
               .fail(function()
                {
                    $('#modal-formulario-body').html('<i class="glyphicon glyphicon-info-sign"></i> Ocurrio un error, por favor intente nuevamente...');
                    $('.selectpicker').selectpicker('refresh');
                });
          }
     }).on('submit', '#frmaction', function(e)
     {
          e.preventDefault();
          $('input[type="submit"]').attr('disabled','disabled');
          $.ajax({
               url: 'lib/ajax/acciones.php',
               type: 'POST',
               data: $("#frmaction").serialize(),
               dataType: 'html'
          })
          .done(function(data)
          {
               $('#contenido-ajax').html(''); // blank before load.
               $('#contenido-ajax').html(data); // load here
               $('.selectpicker').selectpicker('refresh');
          })
          .fail(function()
          {
               $('#contenido-ajax').html('');
               $('.selectpicker').selectpicker('refresh');
          });
          $('#formulario-modal').modal('hide');
     }).on('submit', '#animos', function(e)
     {
          e.preventDefault();
          $('input[type="submit"]').attr('disabled','disabled');
          $.ajax({
               url: 'lib/ajax/acciones.php',
               type: 'POST',
               data: $("#animos").serialize(),
               dataType: 'html'
          })
          .done(function(data)
          {
               $('#contenido-ajax').html(''); // blank before load.
               $('#contenido-ajax').html(data); // load here
          })
          .fail(function()
          {
               $('#contenido-ajax').html('Error al registrar su estado de animo.');
          });
          //$('#formulario-modal').modal('hide');
     }).on('submit', '#filtros', function(e)
     {

          e.preventDefault();
          //alert('enviando formulario');
          //$('input[type="submit"]').attr('disabled','disabled');
          $.ajax({
               url: 'lib/ajax/utilidades.php',
               type: 'POST',
               data: $("#filtros").serialize(),
               dataType: 'html'
          })
          .done(function(data)
          {
               $('#reload').html(''); // blank before load.
               $('#reload').html(data); // load here
          })
          .fail(function()
          {
               $('#reload').html('Error al realizar la consulta');
          });
          //$('#formulario-modal').modal('hide');
     });


     //boton cerrar de la ventana modal
     $(document).on('click', '#modal-formulario-cerrar', function(e)
     {
          //doSomethingCool();
          $('.modal-formulario-titulo').text('');
          $('#modal-formulario-body').text('');
          $('#modal-formulario-accion').text('');
          $('#formulario-modal').modal('hide');
     });
     //boton cerrar de la ventana modal
     $(document).on('click', '#datosanimos-modal-cerrar', function(e)
     {
          //doSomethingCool();
          $('#datosanimos-title').text('');
          $('#datosanimos').text('');
          $('#datosanimos-modal').modal('hide');
     });


     //cambio de estado selector de proyecto
     $(document).on('change', '#idproyectochange', function(e)
     {
          e.preventDefault();

          $.ajax({
               url: 'lib/ajax/utilidades.php',
               type: 'POST',
               data: {accion:'selectEquipo',idproyecto:this.value},
               dataType: 'html'
          })
          .done(function(data)
          {
               $('#reloadequipo').html(''); // blank before load.
               $('#reloadequipo').html(data); // load here
               $('.selectpicker').selectpicker('refresh');
          })
          .fail(function()
          {
               $('#reloadequipo').html('Error al cargar los datos');
          });
          
     });
     
     
    
    $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function(){
            $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true);

    });

});