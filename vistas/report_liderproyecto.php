

<?php
    if(isset($_SESSION["ROL"]))
    {
        if(!isset($url))require './dao/Animo.php';
        else require '../../dao/Animo.php';
        $Archivo=encriptar("vistas/equipos.php");
        if(!isset($url))require './dao/Proyecto.php';
        else require '../../dao/Proyecto.php';
        $conexion=Conexion::conectar();
?>
    <!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Reporte por Equipos
    <small>Panel de control</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Reporte por Equipos</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->

  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <!-- Custom tabs (Charts with tabs)-->
      <div class="nav-tabs-custom">
        <!-- Tabs within a box -->


      </div><!-- /.nav-tabs-custom -->



      <!-- Listado de equipos -->
      <div class="box box-primary">
        <div class="box-header">
          <i class="ion ion-clipboard"></i>
          <h3 class="box-title">Reporte por Equipos</h3>
          <div class="box-tools pull-right">
            <!--<ul class="pagination pagination-sm inline">
              <li><a href="#">&laquo;</a></li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">&raquo;</a></li>
            </ul>-->
          </div>
        </div><!-- /.box-header -->
        <div class="box-body" >
          <div class="row">
            <div class="col-sm-12 col-lg-12">
              <form id='filtros'>
                <div class="form-group">
                  <input type="hidden" name="accion" value="estadosEquipo">
                  <input type="hidden" name="Archivo" value="<?php echo $Archivo;?>">
                  <div class="row">
                    <div class="col-sm-6 col-lg-6">
                      <label for="idproyecto">Seleccione un proyecto:</label>
                      <select class="selectpicker" name="idproyecto" id="idproyectochange" title="Seleccione un proyecto" required>
                        <?php             
                          $lst=Proyecto::getProyectosPorLiderDeProyecto($conexion);
                          foreach ($lst as $l) 
                          {
                            echo '<option value='.$l['id'].'>'.$l['nombre'].'</option>';
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-6 col-lg-6" id="reloadequipo">
                      <label for="idequipo">Seleccione un equipo:</label>
                      <select class="selectpicker" name="idequipo" title="Primero elija un proyecto" required></select>
                    </div>
                    <div class="col-sm-6 col-lg-6"><input type="date" name="fechamin" class="form-control" required />
                    </div>
                    <div class="col-sm-6 col-lg-6"><input type="date" name="fechamax" class="form-control" required /></div>
                    <div class="col-sm-12 col-lg-12">
                      <input type="submit" name="enviar" value="Consultar" class="btn btn-primary">    
                    </div>
                    
                  </div>


                  
                  
                </div>
              </form>
            </div>
          </div>




          <div class="row">
            <div class="col-lg-12" id="reload">
              
            </div>




            
          </div>
        </div><!-- /.box-body -->
        
      </div><!-- /.box -->
      <div class="modal fade" tabindex="-1" role="dialog" id="datosanimos-modal" data-backdrop='static' data-keyboard='false'>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="datosanimos-title"></h4>
                  </div>
                  <div class="modal-body" id="datosanimos">
                      
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal" id="datosanimos-modal-cerrar" >Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
    </section><!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
  
  
</section><!-- /.content -->
    <?php
    }else
    {
        echo "Sin permiso";
    }
?>


