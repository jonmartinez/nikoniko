<?php

    switch ($accion)
    {
        case 'Guardar':
            //guarda el equipo
            adicionar("equipos", $conexion);
            //obtiene el id ***SELECT @@identity AS id
            consultar("SELECT max(id) AS idequipo from equipos where idproyecto=:Idproyecto and nombre=:Nombre",array('Idproyecto'=>$idproyecto,'Nombre'=>$nombre), $conexion);
            //guardar el lider 
            $lider='1';
            $idusuario=$idlider;
            adicionar("miembrosdeequipo", $conexion);
            //guardar el listado de miembros
            $lider='0';
            $idusuario='';
            //recorrer el listado de miembros
            $integrantes = (isset($integrantes) && ($integrantes!='' || $integrantes!=null))?$integrantes:array();
            if(count($integrantes)>0)
            {
                foreach ($integrantes as $idusuario) {
                    if($idusuario!=$idlider) adicionar("miembrosdeequipo",$conexion);
                }
            }
            break;
        case 'Modificar':
            $idequipo=desencriptar($token);
            modificar("equipos", "nombre,idproyecto", "where id=:ID", array('ID'=>$idequipo), $conexion);
            eliminar("miembrosdeequipo", "where idequipo=:ID", array('ID'=> $idequipo ), $conexion);
            //guardar el lider 
            $lider='1';
            $idusuario=$idlider;
            adicionar("miembrosdeequipo", $conexion);
            //guardar el listado de miembros
            $lider='0';
            $idusuario='';
            //recorrer el listado de miembros
            $integrantes = (isset($integrantes) && ($integrantes!='' || $integrantes!=null))?$integrantes:array();
            if(count($integrantes)>0)
            {
                foreach ($integrantes as $idusuario) {
                    if($idusuario!=$idlider) adicionar("miembrosdeequipo",$conexion);
                }
            }
            break;
        case 'Eliminar':
            try {
                eliminar("equipos", "where id=:ID", array('ID'=>  desencriptar($token)), $conexion);    
            } catch (Exception $exc) {
                echo "No fue posible eliminar el registro.";
            }

            
            break;
        
    }
    include ("../../".  desencriptar($url));

?>
