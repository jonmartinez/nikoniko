

<?php
    if(isset($_SESSION["ROL"]))
    {
        if(!isset($url))require './dao/Animo.php';
        else require '../../dao/Animo.php';
        $Archivo=encriptar("vistas/equipos.php");
?>
    <!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Reporte de Usuario
    <small>Panel de control</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Reporte de Usuario</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->

  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <!-- Custom tabs (Charts with tabs)-->
      <div class="nav-tabs-custom">
        <!-- Tabs within a box -->


      </div><!-- /.nav-tabs-custom -->



      <!-- Listado de equipos -->
      <div class="box box-primary">
        <div class="box-header">
          <i class="ion ion-clipboard"></i>
          <h3 class="box-title">Reporte de Usuario</h3>
          <div class="box-tools pull-right">
            <!--<ul class="pagination pagination-sm inline">
              <li><a href="#">&laquo;</a></li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">&raquo;</a></li>
            </ul>-->
          </div>
        </div><!-- /.box-header -->
        <div class="box-body" >
          <div class="row">
            <div class="col-lg-12">
              <div id="myfirstchart"></div>
            </div>
          </div>
        </div><!-- /.box-body -->
        
      </div><!-- /.box -->

    </section><!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
  
  <?php
    $conexion=Conexion::conectar();
    $xs=Animo::getEstadosDeUnUsuario($_SESSION["IDUSUARIO"],$conexion);
    $datos='';
    foreach ($xs as $x) 
    {
      $datos.="{month:'".$x['fecha']."',value:".$x['animo']."},";
    }
    
  ?>
    
  </div><!-- /.row (main row) -->
  <script type="text/javascript">
   new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'myfirstchart',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
    /*{ year: '2008', value: 100 },
    { year: '2009', value: 10 },
    { year: '2010', value: 5 },
    { year: '2011', value: 5 },
    { year: '2012', value: 20 }*/
    <?php echo substr(trim($datos), 0, strlen($datos)-1);?>
  ],
  // The name of the data record attribute that contains x-values.
  xkey: 'month',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['value'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Estado de ánimo (0-5)'],
  ymax:5,
  ymin:0,
  goals:[1,4],
  goalLineColors:['#d14','#02e809']
}).on('click', function(i, row){
                      row.accion='darDatosAnimoUsuario';
                      //console.log(i,row);
              $.ajax({
              url: 'lib/ajax/utilidades.php',
              type: 'POST',
              data: row,
              dataType: 'html'
              })
                    .done(function(data)
                    {
                       $('#datosanimos-title').html(''); // blank before load.
                       $('#datosanimos-title').html('Mi estado de animo para la fecha '+row.month); // load here
                       $('#datosanimos').html(''); // blank before load.
                       $('#datosanimos').html(data); // load here
                       $('#datosanimos-modal').modal('show');
                    })
                    .fail(function()
                    {
                       $('#datosanimos').html('Error al cargar los datos');
                    });
            });
  </script>



  <div class="modal fade" tabindex="-1" role="dialog" id="datosanimos-modal" data-backdrop='static' data-keyboard='false'>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="datosanimos-title"></h4>
                  </div>
                  <div class="modal-body" id="datosanimos">
                      
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal" id="datosanimos-modal-cerrar" >Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
</section><!-- /.content -->
    <?php
    }else
    {
        echo "Sin permiso";
    }
?>


