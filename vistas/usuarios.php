

<?php
    if(isset($_SESSION["ROL"]) && $_SESSION["ROL"]=='0')
    {
        if(!isset($url))require './dao/Usuario.php';
        else require '../../dao/Usuario.php';
        $Archivo=encriptar("vistas/usuarios.php");
?>
    <!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Usuarios
    <small>Panel de control</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Usuarios</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->

  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <!-- Custom tabs (Charts with tabs)-->
      <div class="nav-tabs-custom">
        <!-- Tabs within a box -->


      </div><!-- /.nav-tabs-custom -->



      <!-- Listado de proyectos -->
      <div class="box box-primary">
        <div class="box-header">
          <i class="ion ion-clipboard"></i>
          <h3 class="box-title">Listado de usuarios</h3>
          <div class="box-tools pull-right">
            <!--<ul class="pagination pagination-sm inline">
              <li><a href="#">&laquo;</a></li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">&raquo;</a></li>
            </ul>-->
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <ul class="todo-list">
              
            <?php
                $conexion=Conexion::conectar();
                $usuarios=Usuario::getAll($conexion);
                foreach ($usuarios as $usuario) 
                {
                    echo "<li>";
                    echo '<!-- drag handle -->
                            <span class="handle">
                              <i class="fa fa-ellipsis-v"></i>
                              <i class="fa fa-ellipsis-v"></i>
                            </span>
                            <!-- checkbox -->
                            <input type="checkbox" value="'.$usuario['id'].'" name="user"/>';
                    
                    echo '<span class="text">'.$usuario['nombres'].' '. $usuario['apellidos'] .'</span>';
                    //echo "<td>".$usuario['estado']."</td>";
                    echo '<!-- Emphasis label -->
                            <!-- General tools such as edit or delete-->
                            <div class="tools">';
                              echo "<i class='fa fa-edit btn-mostrar-formulario'"
                            .'data-toggle="modal" data-target="#formulario-modal"'
                            . "data-accion='Modificar'"
                            .'data-url="'. $Archivo .'"'
                            ."data-token='".encriptar($usuario['id'])."'"
                            ."data-title='Modificar Usuario'></i>";
                            echo "<i class='fa fa-trash-o Eliminar'"
                                .'data-url="'. $Archivo .'"'
                                ."data-token='".encriptar($usuario['id'])."'"
                                . ">";
                            echo "</i>";
                    echo'</div>';
                    echo "</li>";
                }
                $conexion=null;
            ?>
           
            
            
          </ul>
        </div><!-- /.box-body -->
        <div class="box-footer clearfix no-border">
            <button class="btn btn-default pull-right btn-mostrar-formulario" data-toggle="modal" data-target="#formulario-modal"
                    data-accion="Guardar" 
                    data-url="<?php echo $Archivo;?>"
                    data-title="Nuevo Usuario">
                    <i class="fa fa-plus"></i> Añadir Usuario
            </button>
        </div>
        
        
      </div><!-- /.box -->



    </section><!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    
  </div><!-- /.row (main row) -->
</section><!-- /.content -->
    <?php
    }else
    {
        echo "Sin permiso";
    }
?>


