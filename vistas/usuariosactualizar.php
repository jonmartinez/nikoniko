<?php

    switch ($accion)
    {
        case 'Guardar':
            adicionar("usuarios", $conexion);
            break;
        case 'Modificar':
            modificar("usuarios", "nombres,apellidos,email,usuario,estado,rol", "where id=:ID", array('ID'=>desencriptar($token)), $conexion);
            break;
        case 'Eliminar':
            eliminar("usuarios", "where id=:ID", array('ID'=>  desencriptar($token)), $conexion);
            break;
        
    }
    include ("../../".  desencriptar($url));

?>
