
<?php
    if(isset($_SESSION["ROL"]))
    {
        if(!isset($url))require './dao/Usuario.php';
        else require '../../dao/Usuario.php';
        $Archivo=encriptar("vistas/registraranimo.php");
?>
    <!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Registrar Estado de Ánimo
    <small>Usuario</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Registrar mi estado</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->

  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <!-- Custom tabs (Charts with tabs)-->
      <div class="nav-tabs-custom">
        <!-- Tabs within a box -->


      </div><!-- /.nav-tabs-custom -->



      <!-- Listado de proyectos -->
      <div class="box box-primary">
        <div class="box-header">
          <i class="ion ion-clipboard"></i>
          <h3 class="box-title">Registar</h3>
          <div class="box-tools pull-right">
            
          </div>
        </div><!-- /.box-header -->
        <form id="animos" name="animos">
        <div class="box-body">
            <?php
                $conexion=Conexion::conectar();
                $num=Usuario::existeAnimoDeUsuario($conexion);
                if($num==0)
                {
            ?>
            <div class="row form-group product-chooser">
                
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                    <div class="product-chooser-item">
                        <img src="lib/emoticones/0.png" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Muy triste">
                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                            <span class="title"></span>
                            <input type="radio" name="animo" value="0">
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                    <div class="product-chooser-item">
                        <img src="lib/emoticones/1.png" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Triste">
                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                            <span class="title"></span>
                            <input type="radio" name="animo" value="1">
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                    <div class="product-chooser-item">
                        <img src="lib/emoticones/2.png" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Decaído">
                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                            <span class="title"></span>
                            <input type="radio" name="animo" value="2">
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                    <div class="product-chooser-item selected">
                        <img src="lib/emoticones/3.png" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Normal">
                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                            <span class="title"></span>
                            <input type="radio" name="animo" value="3">
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                    <div class="product-chooser-item ">
                        <img src="lib/emoticones/4.png" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Alegre">
                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                            <span class="title"></span>
                            <input type="radio" name="animo" value="4" checked>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                    <div class="product-chooser-item ">
                        <img src="lib/emoticones/5.png" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Feliz">
                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                            <span class="title"></span>
                            <input type="radio" name="animo" value="5">
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="detalle" class="col-form-label">Fecha:</label>
                    <input type="date" class="form-control" name="fecha" value="<?php echo date('Y-m-d'); ?>" required>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="detalle" class="col-form-label">Detalle su estado de ánimo:</label>
                    <textarea class="form-control" name="detalle" ></textarea>
                </div>
            </div>
            <?php
                }else
                {
                    ?>
                         <div class="row form-group product-chooser">
                
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="product-chooser-item">
                                    <img src="lib/emoticones/like.png" class="img-rounded col-xs-6 col-sm-6 col-md-12 col-lg-12" alt="Estado registrado">
                                    <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                        <h4 class="alert alert-success" style="text-align: center;">Ya ha registrado el estado de animo para el día de hoy</h4>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            </div>
                        </div>
                    <?php
                }
            ?>
        
        </div><!-- /.box-body -->
        <?php if ($num==0) {
            # code...
        ?>
        <div class="box-footer clearfix no-border">
            <input type="hidden" name="accion" value="Guardar">
            <input type="hidden" name="url" value="<?php echo $Archivo; ?>">

            <input type="submit" class="btn btn-primary pull-right" value='Guardar estado de animo' name='guardarEstado'>
        </div>
        <?php
        }
        ?>
        </form>
        
      </div><!-- /.box -->



    </section><!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    
  </div><!-- /.row (main row) -->
</section><!-- /.content -->
    
    <?php
    }else
    {
        echo "Sin permiso";
    }
?>


