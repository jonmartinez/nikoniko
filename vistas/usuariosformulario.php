<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    $id='';$nombres=''; $apellidos=''; $email=''; $usuario=''; $estado=''; $rol='';
    
    if(isset($token))
    {
        consultar("select id,nombres,apellidos,email,usuario,estado,rol from usuarios where id=:ID",array('ID'=>desencriptar($token)), $conexion);
    }
    
 ?>
    <div class="form-group row">
        <label for="nombres" class="col-sm-2 col-form-label">Nombres</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombres" value="<?php echo $nombres;?>" required>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="apellidos" class="col-sm-2 col-form-label">Apellidos</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="nombres" name="apellidos" placeholder="Apellidos" value="<?php echo $apellidos;?>" required>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="email" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $email;?>" required>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="usuario" class="col-sm-2 col-form-label">Usuario</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Username" value="<?php echo $usuario;?>" required>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="password" name="password" placeholder="*********" required>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="estado" class="col-sm-2 col-form-label">Estado</label>
        <div class="col-sm-10">
            <input type="radio" class="form-check-input" id="estado" name="estado" value="1" <?php echo ($estado=='1')?'checked':''; ?> required>Activo<br>
            <input type="radio" class="form-check-input" id="estado" name="estado" value="0" <?php echo ($estado=='0' || $estado=='')?'checked':''; ?> required>Inactivo
        </div>
    </div>
    
    
    <div class="form-group row">
        <label for="rol" class="col-sm-2 col-form-label">Rol</label>
        <div class="col-sm-10">
            <input type="radio" class="form-check-input" id="rol" name="rol" value="1" <?php echo ($estado=='0' || $estado=='')?'checked':''; ?> required>Usuario<br>
            <input type="radio" class="form-check-input" id="rol" name="rol" value="0" <?php echo ($estado=='1')?'checked':''; ?> required>Administrador
        </div>
    </div>
    <div class="form-group row">
            <input type="hidden"  id="url" name="url" value="<?php echo $url;?>">
            <input type="hidden"  id="token" name="token" value="<?php echo encriptar($id);?>">
            <input type="hidden"  id="accion" name="accion">
    </div>
    
    
    
