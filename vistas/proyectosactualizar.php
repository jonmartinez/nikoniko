<?php

    switch ($accion)
    {
        case 'Guardar':
            adicionar("proyectos", $conexion);
            break;
        case 'Modificar':
            modificar("proyectos", "nombre,descripcion,idusuario", "where id=:ID", array('ID'=>desencriptar($token)), $conexion);
            break;
        case 'Eliminar':
            eliminar("proyectos", "where id=:ID", array('ID'=>  desencriptar($token)), $conexion);
            break;
        
    }
    include ("../../".  desencriptar($url));

?>
