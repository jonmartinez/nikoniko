

<?php
    if(isset($_SESSION["ROL"]) && $_SESSION["ROL"]=='0')
    {
        if(!isset($url))require './dao/Equipo.php';
        else require '../../dao/Equipo.php';
        $Archivo=encriptar("vistas/equipos.php");
        if(!isset($url))require './dao/Proyecto.php';
        else require '../../dao/Proyecto.php';
        
        $conexion=Conexion::conectar();
?>
    <!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Usuarios
    <small>Panel de control</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Equipos</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->

  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <!-- Custom tabs (Charts with tabs)-->
      <div class="nav-tabs-custom">
        <!-- Tabs within a box -->


      </div><!-- /.nav-tabs-custom -->



      <!-- Listado de equipos -->
      <div class="box box-primary">
        <div class="box-header">
          <i class="ion ion-clipboard"></i>
          <h3 class="box-title">Listado de equipos</h3>
          <div class="box-tools pull-right">
            <!--<ul class="pagination pagination-sm inline">
              <li><a href="#">&laquo;</a></li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">&raquo;</a></li>
            </ul>-->
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-sm-12 col-lg-12">
              <form id='filtros'>
                <div class="form-group">
                  <input type="hidden" name="accion" value="listarEquipos">
                  <input type="hidden" name="Archivo" value="<?php echo $Archivo;?>">
                  <label for="idproyecto">Seleccione un proyecto:</label>
                  <select class="selectpicker" name="idproyecto" id="idproyecto" title="Seleccione un proyecto" required>
                    <?php             
                      $lst=Proyecto::getProyectosPorLiderDeProyecto($conexion);
                      foreach ($lst as $l) 
                      {
                        echo '<option value='.$l['id'].'>'.$l['nombre'].'</option>';
                      }
                    ?>
                  </select>
                  <input type="submit" name="enviar" value="Consultar" class="btn btn-primary">
                </div>
              </form>
            </div>
          </div>



          <div class="row">
            <div class="col-sm-12 col-lg-12" id="reload">
              
            </div>
          </div>
        </div><!-- /.box-body -->
        <div class="box-footer clearfix no-border">
            <button class="btn btn-default pull-right btn-mostrar-formulario" data-toggle="modal" data-target="#formulario-modal"
                    data-accion="Guardar" 
                    data-url="<?php echo $Archivo;?>"
                    data-title="Nuevo Equipo">
                    <i class="fa fa-plus"></i> Añadir Equipo
            </button>
        </div>
        
        
      </div><!-- /.box -->



    </section><!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    
  </div><!-- /.row (main row) -->
</section><!-- /.content -->
    <?php
    }else
    {
        echo "Sin permiso";
    }
?>


