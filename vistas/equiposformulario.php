<?php
require '../../dao/Equipo.php';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    $id='';$nombre='';  $idproyecto='';$idlider=''; $integrantes=null;
    
    if(isset($token))
    {
        consultar("select id,nombre,idproyecto from equipos where id=:ID",array('ID'=>desencriptar($token)), $conexion);
        //consultar el id del lider del equipo
        consultar("select idusuario idlider from miembrosdeequipo where idequipo=:ID and lider=1",array('ID'=>$id), $conexion);
        //consulta los interantes
        $integrantes=Equipo::getIdsIntegrantesEquipo($id, $conexion);
        //print_r($integrantes);
    }
    
 ?>
    <div class="form-group row">
        <label for="idproyecto" class="col-sm-2 col-form-label">Proyecto</label>
        <div class="col-sm-10">
            <?php
                echo selectoption('id',"nombre","proyectos","",null,$idproyecto,'name="idproyecto" id="idproyecto" class="selectpicker" data-live-search="true" data-max-options="1" title="Seleccione un proyecto" required',$conexion);
            ?>
        </div>
    </div>
    
    
    <div class="form-group row">
        <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $nombre;?>" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="idlider" class="col-sm-2 col-form-label">Lider del Equipo</label>
        <div class="col-sm-10">
            <?php
                echo selectoption('id',"concat(nombres,' ',apellidos) usuario","usuarios","",null,$idlider,'name="idlider" id="idlider" class="selectpicker" data-live-search="true" data-max-options="1" title="Seleccione un líder" required',$conexion);
            ?>
        </div>
    </div>
    <div class="form-group row">
        <label for="integrantes[]" class="col-sm-2 col-form-label">Integrantes del Equipo</label>
        <div class="col-sm-10">
            <?php
                echo selectoptionValueArray('id',"concat(nombres,' ',apellidos) usuario","usuarios","",null,$integrantes,'name="integrantes[]" id="integrantes[]" multiple class="selectpicker" data-live-search="true"  title="Seleccione los integrantes" required',$conexion);
            ?>
        </div>
    </div>
    
    
    
   
    <div class="form-group row">
            <input type="hidden"  id="url" name="url" value="<?php echo $url;?>">
            <input type="hidden"  id="token" name="token" value="<?php echo encriptar($id);?>">
            <input type="hidden"  id="accion" name="accion">
    </div>
    
    
    
