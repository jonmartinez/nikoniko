<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    $id='';$nombre=''; $descripcion=''; $idusuario='';
    
    if(isset($token))
    {
        consultar("select id,nombre,descripcion,idusuario from proyectos where id=:ID",array('ID'=>desencriptar($token)), $conexion);
    }
    
 ?>
    <div class="form-group row">
        <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $nombre;?>"  required>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="descripcion" class="col-sm-2 col-form-label">Descripción</label>
        <div class="col-sm-10">
            <textarea class="form-control"  name="descripcion" id="descripcion" required><?php echo $descripcion;?></textarea>
        </div>
    </div>

    <div class="form-group row">
        <label for="idusuario" class="col-sm-2 col-form-label">Líder de Proyecto</label>
        <div class="col-sm-10">
            <?php
                echo selectoption('id',"concat(nombres,' ',apellidos) usuario","usuarios","",null,$idusuario,'name="idusuario" id="idusuario" class="selectpicker" data-live-search="true" data-max-options="1" title="Seleccione un líder" required',$conexion);
            ?>
        </div>
    </div>
    
    
    
   
    <div class="form-group row">
            <input type="hidden"  id="url" name="url" value="<?php echo $url;?>">
            <input type="hidden"  id="token" name="token" value="<?php echo encriptar($id);?>">
            <input type="hidden"  id="accion" name="accion">
    </div>
    
    
    
