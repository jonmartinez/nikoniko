<html>
    <head>
        <?php $theme="./theme/default/";?>
        <meta charset="UTF-8">
        <title>NikoNiko</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="<?php echo $theme;?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
        <!-- FontAwesome 4.3.0 -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons 2.0.0 -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
        <!-- Theme style -->
        <link href="<?php echo $theme;?>dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <link href="<?php echo $theme;?>dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo $theme;?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <link href="lib/css/botones.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="index2.html" class="logo"><b>Proyecto </b>NIKONIKO</a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                  <!-- Sidebar toggle button-->
                  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                  </a>
                  <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                      
                      <!-- User Account: style can be found in dropdown.less -->
                      <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <!--<img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"/>-->
                          <span class="hidden-xs"><?php echo $_SESSION["NOMBRES"].' '.$_SESSION["APELLIDOS"];?></span>
                        </a>
                        <ul class="dropdown-menu">
                          <!-- User image -->
                          <li class="user-header">
                            <!--<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />-->
                            <p>
                              <?php echo $_SESSION["NOMBRES"].' '.$_SESSION["APELLIDOS"];?>
                              <small><?php echo ($_SESSION["ROL"]=='0')?'Administrador':'Usuario';?></small>
                            </p>
                          </li>
                          <!-- Menu Body -->
                          <!-- Menu Footer-->
                          <li class="user-footer">
                            <div class="pull-left">
                              <a href="#" class="btn btn-default btn-flat">Perfil</a>
                            </div>
                            <div class="pull-right">
                              <a href="salir.php" class="btn btn-default btn-flat">Salir</a>
                            </div>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </nav>
              </header>
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                  <!-- Sidebar user panel -->
                  <div class="user-panel">
                    <div class="pull-left image">
                      <!--<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />-->
                    </div>
                    <div class="pull-left info">
                      <p><?php echo $_SESSION["NOMBRES"].' '.$_SESSION["APELLIDOS"];?></p>

                      <a href="#"><i class="fa fa-circle text-success"></i> En Linea</a>
                    </div>
                  </div>
                  <!-- search form -->
                  
                  <!-- /.search form -->
                  <!-- sidebar menu: : style can be found in sidebar.less -->
                  <?php include './theme/default/menu.php';?>
                  
                </section>
                <!-- /.sidebar -->
              </aside>
            <!-- Right side column. Contains the navbar and content of the page -->
            <div class="content-wrapper" id="contenido-ajax">
                <?php 
                    include("./".desencriptar($_GET['Info']));
                ?>
            </div><!-- /.content-wrapper -->   
        </div>
        
        <div class="modal fade" tabindex="-1" role="dialog" id="formulario-modal" data-backdrop='static' data-keyboard='false'>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form method="POST" name="frmaction" id="frmaction">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="modal-formulario-title"></h4>
                        </div>
                        <div class="modal-body" id="modal-formulario-body">
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="modal-formulario-cerrar" >Cerrar</button>
                            <input type="submit" class="btn btn-primary" id="accion-frm" value="">
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        
        
        <!-- jQuery 2.1.3 -->
        <script src="<?php echo $theme;?>plugins/jQuery/jQuery-2.1.3.min.js"></script>
        <script src="./lib/js/nikoniko.js" type="text/javascript"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/i18n/defaults-es_ES.min.js" type="text/javascript"></script>
        <!-- jQuery UI 1.11.2 -->
        <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
          $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="<?php echo $theme;?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
        <!-- AdminLTE App -->
        <script src="<?php echo $theme;?>dist/js/app.min.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?php echo $theme;?>dist/js/pages/dashboard.js" type="text/javascript"></script>

        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo $theme;?>dist/js/demo.js" type="text/javascript"></script>
        
        
        
    </body>
</html>