<ul  class="sidebar-menu">
    <li class="header">Navegador</li>
    <li class="active">
        <a href="?Info=<?php echo encriptar("bienvenido.php");?>"><span class="fa fa-dashboard"></span> <span>Inicio</span> </a>
    </li>
    <?php 
        $activo1='';
        $activo2='';
        $activo3='';
        $rutaActual=desencriptar($_GET['Info']);
        if($rutaActual=="/vistas/usuarios.php" || $rutaActual=="/vistas/proyectos.php" || $rutaActual=="/vistas/equipos.php" )
        {
            $activo1='active';
            $activo2='';
            $activo3='';
        }else if($rutaActual=="/vistas/registraranimo.php")
        {
            $activo2='active';
            $activo1='';
            $activo3='';
        }elseif ($rutaActual=="/vistas/report_liderproyecto.php" || $rutaActual=="/vistas/report_usuario.php") {
            $activo3='active';
            $activo1='';
            $activo2='';
        }
    ?>
    <?php
         if(isset($_SESSION["ROL"]) && $_SESSION["ROL"]=='0')
    {
    ?>
    <li class="<?php echo $activo1;?> treeview">
        <a href="#">
            <i class="fa fa-dashboard"></i> <span>Panel de control</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li <?php echo $act=($rutaActual=="/vistas/usuarios.php")?"class='active'":''; ?>><a href="?Info=<?php echo encriptar("/vistas/usuarios.php");?>"><i class="fa fa-circle-o"></i> Usuarios</a></li>
            <li <?php echo $act=($rutaActual=="/vistas/proyectos.php")?"class='active'":''; ?>><a href="?Info=<?php echo encriptar("/vistas/proyectos.php");?>"><i class="fa fa-circle-o"></i> Proyectos</a></li>
            <li <?php echo $act=($rutaActual=="/vistas/equipos.php")?"class='active'":''; ?>><a href="?Info=<?php echo encriptar("/vistas/equipos.php");?>"><i class="fa fa-circle-o"></i> Equipos</a></li>
        </ul>
    </li>
    <?php
    }
    ?>
    <li class="<?php echo $activo2;?> treeview">
        <a href="#">
            <i class="fa fa-dashboard"></i> <span>Usuario</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="active treeview-menu">
            <li <?php echo $act=($rutaActual=="/vistas/registraranimo.php")?"class='active'":''; ?>><a href="?Info=<?php echo encriptar("/vistas/registraranimo.php");?>"><i class="fa fa-circle-o"></i> Registrar mi estado</a></li>
        </ul>
    </li>
    <li class="<?php echo $activo3;?> treeview">
        <a href="#">
            <i class="fa fa-dashboard"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="active treeview-menu">
            <li <?php echo $act=($rutaActual=="/vistas/report_liderproyecto.php")?"class='active'":''; ?>><a href="?Info=<?php echo encriptar("/vistas/report_liderproyecto.php");?>"><i class="fa fa-circle-o"></i>Reporte por Equipos</a></li>
            <li <?php echo $act=($rutaActual=="/vistas/report_usuario.php")?"class='active'":''; ?>><a href="?Info=<?php echo encriptar("/vistas/report_usuario.php");?>"><i class="fa fa-circle-o"></i>Reporte usuario</a></li>
        </ul>
    </li>
    
    
    
    
    
</ul>
