<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Usuario
{
    public static function getAll($conexion)
    {
        
        $cadena="select * from usuarios";
        return consultararray($cadena,null,$conexion);
        
    }
    
    public static function getUsuario($array,$conexion)
    {
        
        $cadena="select * from usuarios where ";
        foreach ($array as $clave=>$key){ $cadena.=strtolower($clave).'=:'.$clave. ' and '; }
        $cadena=substr($cadena, 0, -5);
        return consultararray($cadena,$array,$conexion);
        
    }
    
    public static function getUsuarioPersonalizado($cadena,$array,$conexion)
    {
        return consultararray($cadena,$array,$conexion);
    }

    public static function existeAnimoDeUsuario($conexion)
    {
        $cadena="select idusuario from animos where fecha=curdate() and idusuario=".$_SESSION["IDUSUARIO"];
        return contarRegistros($cadena,$conexion);
    }


}
?>
