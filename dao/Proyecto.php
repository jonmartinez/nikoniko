<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Proyecto
{
    public static function getAll($conexion)
    {
        
        $cadena="select * from proyectos";
        return consultararray($cadena,null,$conexion);
        
    }

    /**
    *   obtiene el lisado de proyectos del lider del proyecto
    *   muestra todos los proyectos para el rol=0 que es administrador
    */
    public static function getProyectosPorLiderDeProyecto($conexion)
    {
        
        $cadena=(isset($_SESSION["ROL"]) && $_SESSION["ROL"]=='0')?"select * from proyectos":"SELECT * from proyectos where idusuario=".$_SESSION["IDUSUARIO"];
        return consultararray($cadena,null,$conexion);
        
    }
    
    public static function getUsuario($array,$conexion)
    {
        
        $cadena="select * from proyectos where ";
        foreach ($array as $clave=>$key){ $cadena.=strtolower($clave).'=:'.$clave. ' and '; }
        $cadena=substr($cadena, 0, -5);
        return consultararray($cadena,$array,$conexion);
        
    }
    
    public static function getProyectoPersonalizado($cadena,$array,$conexion)
    {
        return consultararray($cadena,$array,$conexion);
    }
}
?>
