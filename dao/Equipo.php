<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Equipo
{
    public static function getAll($conexion)
    {
        
        $cadena="select * from equipos";
        return consultararray($cadena,null,$conexion);
        
    }
    
    public static function getEquiposProyecto($array,$conexion)
    {
        
        $cadena="select e.id id,p.nombre proyecto, e.nombre equipo "
                . "from equipos as e "
                . "inner join proyectos as p on (e.idproyecto=p.id) "
                . "where p.id=:IDP";
        return consultararray($cadena,$array,$conexion);
        
    }
    
    public static function getIdsIntegrantesEquipo($idequipo,$conexion)
    {
        
        $cadena="select idusuario from miembrosdeequipo where idequipo=:IDEQUIPO and lider=0";
        return consultararray($cadena,array('IDEQUIPO'=>$idequipo),$conexion);
        
    }
    
    public static function getEquipoPersonalizado($cadena,$array,$conexion)
    {
        return consultararray($cadena,$array,$conexion);
    }
}
?>
