<?php
    function adicionar($tabla,$conexion)
    {
        try 
        {
            //averiguacion de los nombres de los campos que componen la tabla
            $cadena="select * from $tabla limit 1";
            $sql = $conexion->prepare($cadena);
            $sql->execute();
            //$resultado = $sql->fetchAll();
            $total_column = $sql->columnCount();
            $cadena="insert into $tabla";
            $campos='';
            $valores='';
            
            for ($i=0;$i<$total_column;$i++) 
            {
                $meta = $sql->getColumnMeta($i);
                global ${$meta['name']};
                global ${$meta['native_type']};
                if ($campos!='') $campos.=',';
                $campos.=$meta['name'];
                if ($valores!='') $valores.=',';
                $valores.=":" . ucfirst($meta['name']);
            }

            $cadena.=" ($campos) values ($valores)";
            //echo $cadena . "<br>";
            $camposx=explode(',',$campos);
            foreach ($camposx as $variable) 
            {
                global ${$variable};
                $array[ucfirst($variable)]=(${$variable}!='' || ${$variable}!=null)?${$variable}:null;
            }
            $sql = $conexion->prepare($cadena);
            $sql->execute($array);
        }catch (PDOException $e) {
                echo 'Ocurrio un error al adicionar el registro'.$e->getMessage();
        }

    }


    function consultararray($cadena,$array,$conexion)
    {
        try {
            $sql = $conexion->prepare($cadena);
            $sql->execute($array);
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;

        } catch (PDOException $e) {
                return false;
        }
    }
    
    
    //Consultar un registro y generar las valirables de acuerdo al nombre de la columna
    function consultar($cadena,$array,$conexion)
    {
        //echo $cadena;
        try 
        {
            $sql = $conexion->prepare($cadena);
            $sql->execute($array);
            $resultado = $sql->fetchAll();
            $num=$sql->rowCount();
                $total_column = $sql->columnCount();
            if ($num==0)
            {
                    echo 'No se encontro el registro consultado';
            }elseif ($num==1) {
                foreach ($resultado as $fila) 
                {

                    for ($i=0;$i<$total_column;$i++) 
                    {
                            $meta = $sql->getColumnMeta($i);
                            global ${$meta['name']};//generacion de una variable global
                            //echo $meta['name'];
                            ${$meta['name']}=$fila[$meta['name']];
                    }
                }
            }else{
                    echo 'La consulta arroja más de un registro';
            }
        } catch (PDOException $e) 
        {
                echo 'Error: Ocurrió un error al consultar el registro. '. $e;
        }
    }
    //modificar un registro y generar las valirables de acuerdo al nombre de la columna
    function modificar($tabla,$campos,$filtro,$array,$conexion)
    {
        try 
        {
            $cadena="select $campos from $tabla $filtro";
            $sql = $conexion->prepare($cadena);
            $sql->execute($array);
            $resultado = $sql->fetchAll();
            $num=$sql->rowCount();
            $total_column = $sql->columnCount();
            

            $cadena="update $tabla set ";
            $cadenaActual="update $tabla set ";
            $campos=explode(',',$campos);
            foreach ($campos as $variable) 
            {
                    global ${$variable};
                    $cadena.="$variable=:" .ucfirst($variable) . ",";
                    $array[ucfirst($variable)]=(${$variable}!='' || ${$variable}!=null)?${$variable}:null;
                    //cadena a ejecutar cambios
                    $cadenaActual.="$variable='".$array[ucfirst($variable)]."',";
            }
            $cadena=substr($cadena,0,strlen($cadena)-1);//borra la coma final
            $cadenaActual=substr($cadenaActual,0,strlen($cadenaActual)-1);//borra la coma final
            $cadena.=" $filtro";
            $cadenaActual.=" where ";
            //echo $cadenaActual;
            $sql = $conexion->prepare($cadena);
            $sql->execute($array);
            
        } catch (PDOException $e) {
                echo 'Error: Ocurrió un error al modificar el/los registros.<br>'.$e->getMessage();
        }
    }
    
    
    function eliminar($tabla,$filtro,$array,$conexion)
    {
        try 
        {
            $cadena="delete from $tabla $filtro";
            $sql = $conexion->prepare($cadena);
            $sql->execute($array);
        } catch (PDOException $e) {
                echo '<b>Error:</b> Ocurrió un error al eliminar el registro. ';
        }
    }
    
    //funcion para lenar lista de un select (select simple)
    function selectoption($llave,$campo,$tabla,$filtro,$array,$valor,$attrs_select,$conexion)
    {
        try 
        {
            $ex=  explode(' ', $campo);
            $alias=  end($ex);
            $cadena="select $llave,$campo from $tabla $filtro order by $alias";
            $sql = $conexion->prepare($cadena);
            if($array==null) $sql->execute();
            else $sql->execute($array);
            $resultado = $sql->fetchAll();
            echo "<select $attrs_select>";
            foreach ($resultado as $fila) 
            {
                if ($valor==$fila[$llave]) $adicional='selected';
                else $adicional='';
                echo '<option value=' . $fila[$llave] . " $adicional>" . $fila[$alias];
            }			
            echo "</select>";
        } catch (PDOException $e) {
                echo 'Error: Ocurrió un error al cargar la lista. '.$e->getMessage();
        }
    }
    
     //funcion para lenar lista de un select (select simple)
    function selectoptionValueArray($llave,$campo,$tabla,$filtro,$array,$valores,$attrs_select,$conexion)
    {
        try 
        {
            $ex=  explode(' ', $campo);
            $alias=  end($ex);
            $cadena="select $llave,$campo from $tabla $filtro order by $alias";
            $sql = $conexion->prepare($cadena);
            if($array==null) $sql->execute();
            else $sql->execute($array);
            $resultado = $sql->fetchAll();
            $cadena= "<select $attrs_select>";
            //echo count($valores);
            foreach ($resultado as $fila) 
            {
                $adicional='';
                if(count($valores)>0 && $valores!=null)
                {
                    $encontrado=false;
                    for($i=0;$i<count($valores) && $encontrado==false;$i++)
                    {
                        if(in_array($fila[$llave], $valores[$i]))
                        {
                            $adicional='selected';
                            $encontrado=true;
                        }else { $adicional='';}
                    }
                    
                }
                $cadena.= '<option value=' . $fila[$llave] . " $adicional>" . $fila[$alias];
            }           
            $cadena.="</select>";
            return $cadena;
        } catch (PDOException $e) {
                return 'Error: Ocurrió un error al cargar la lista. '.$e->getMessage();
        }
    }//funcion para lenar lista de un select (select simple)
    function contarRegistros($cadena,$conexion)
    {
        try 
        {
            //echo $cadena;
            $sql = $conexion->prepare($cadena);
            $sql->execute();
            $resultado = $sql->fetchAll();
            return count($resultado);
        } catch (PDOException $e) {
                return 'Error: Ocurrió al contar el numero de registros. <br>'.$e->getMessage();
        }
    }
    
    function encriptar($cadena){
        $key="nikonikov1";  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $cadena, MCRYPT_MODE_CBC, md5(md5($key))));
        $encrypted=urlencode($encrypted);
        return $encrypted; //Devuelve el string encriptado

    }

    function desencriptar($cadena){
        $cadena=urldecode($cadena);
        $cadena=str_replace(' ', '+', $cadena);
        $key="nikonikov1";  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($cadena), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
        return $decrypted;  //Devuelve el string desencriptado
    }
?>