-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-05-2017 a las 13:31:00
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `nikoniko`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `animos`
--

CREATE TABLE IF NOT EXISTS `animos` (
  `idusuario` int(11) NOT NULL,
  `animo` int(11) NOT NULL,
  `detalle` text NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`idusuario`,`fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `animos`
--

INSERT INTO `animos` (`idusuario`, `animo`, `detalle`, `fecha`) VALUES
(1, 5, '', '2017-05-18'),
(1, 0, '', '2017-05-21'),
(1, 3, '', '2017-05-22'),
(1, 4, '', '2017-05-23'),
(1, 5, '', '2017-05-24'),
(1, 4, '', '2017-05-26'),
(1, 5, 'gggggg', '2017-05-27'),
(1, 5, '', '2017-05-29'),
(7, 3, '', '2017-05-18'),
(7, 1, '', '2017-05-23'),
(7, 4, '', '2017-05-24'),
(7, 1, '', '2017-05-25'),
(7, 4, '', '2017-05-30'),
(8, 0, '', '2017-05-18'),
(8, 2, '', '2017-05-21'),
(8, 0, '', '2017-05-24'),
(8, 2, '', '2017-05-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE IF NOT EXISTS `equipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idproyecto` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `equipos-proyectos_idx` (`idproyecto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id`, `idproyecto`, `nombre`) VALUES
(13, 4, 'Equipo 1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembrosdeequipo`
--

CREATE TABLE IF NOT EXISTS `miembrosdeequipo` (
  `idequipo` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `lider` tinyint(1) NOT NULL,
  PRIMARY KEY (`idequipo`,`idusuario`),
  KEY `miembros-usuario_idx` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `miembrosdeequipo`
--

INSERT INTO `miembrosdeequipo` (`idequipo`, `idusuario`, `lider`) VALUES
(13, 1, 1),
(13, 7, 0),
(13, 8, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE IF NOT EXISTS `proyectos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`),
  KEY `proyectos-usuario_idx` (`idusuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`id`, `idusuario`, `nombre`, `descripcion`) VALUES
(4, 1, 'Proyecto 1', 'Proyecto agieles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(80) NOT NULL,
  `apellidos` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `rol` varchar(1) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombres`, `apellidos`, `email`, `usuario`, `password`, `rol`, `estado`) VALUES
(1, 'Jonatan', 'Martinez', 'devjonatan@gmail.com', 'jonmartinez', '123', '0', 1),
(7, 'Juan', 'Arenas', 'asa@mail.com', 'juan', '123', '1', 1),
(8, 'Pedro', 'Perez', 'pedro@mail.com', 'pedro', '123', '1', 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `animos`
--
ALTER TABLE `animos`
  ADD CONSTRAINT `animo-usuarios` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD CONSTRAINT `equipos-proyectos` FOREIGN KEY (`idproyecto`) REFERENCES `proyectos` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `miembrosdeequipo`
--
ALTER TABLE `miembrosdeequipo`
  ADD CONSTRAINT `miembros-equipo` FOREIGN KEY (`idequipo`) REFERENCES `equipos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `miembros-usuario` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD CONSTRAINT `proyectos-usuario` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
